<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth/login');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/pas/dashboard', [App\Http\Controllers\Pas::class, 'index'])->name('home');
//Route::get('pas/dashboard', 'App\Http\Controllers\Pas@index');
Route::get('chauffeur/dashboard', 'App\Http\Controllers\Chauffeur@index');
Route::get('overzicht/inkomsten', 'App\Http\Controllers\Pas@overzichtInkomsten');
Route::get('medewerkers/dashboard', 'App\Http\Controllers\Medewerker@index');
//KlantController
Route::get('klant/dashboard', [App\Http\Controllers\Klant::class, 'index'])->name('index');
Route::get('mijn-pakketen', 'App\Http\Controllers\Klant@getPakket');
Route::get('pakket-aanmelden', 'App\Http\Controllers\Klant@addPakket');
Route::post('pakket-aanmelden', 'App\Http\Controllers\Klant@store');
//MedewerkerController
Route::get('medewerker/dashboard', 'App\Http\Controllers\Medewerker@index');
Route::get('chauffeur/verwijderen/{id}', 'App\Http\Controllers\Pas@chauffeurDelete');
Route::get('your/deleted', 'App\Http\Controllers\chauffeur@yourDeleted');
//ChauffeurController
Route::get('aangemeld-pakketten', 'App\Http\Controllers\Chauffeur@aangemeldPakketten');
Route::get('aangenomen-pakketten', 'App\Http\Controllers\Chauffeur@aangenomenPakketten');
Route::get('pakket/aannemen/{id}', 'App\Http\Controllers\Chauffeur@neemPakket');
Route::post('status/veranderen', 'App\Http\Controllers\Chauffeur@statusVeranderen');

