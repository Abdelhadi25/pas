<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePakkettenTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pakketten', function (Blueprint $table) {
            $table->increments('id');
            $table->bigInteger('klant_id')->unsigned();
            $table->foreign('klant_id')->references('id')->on('klanten');
            $table->Integer('status_id')->unsigned();
            $table->foreign('status_id')->references('id')->on('pakket_status');
            $table->Integer('chauffeur_id')->unsigned()->nullable();
            $table->foreign('chauffeur_id')->references('id')->on('medewerkers');
            $table->string('ophaal_straat', 50);
            $table->string('ophaal_huisnummer', 50);
            $table->string('ophaal_postcode', 10);
            $table->string('ophaal_woonplaats', 10);
            $table->string('bezorg_straat', 50);
            $table->string('bezorg_huisnummer', 50);
            $table->string('bezorg_postcode', 10);
            $table->string('bezorg_woonplaats', 10);
            $table->integer('afmeting_id')->unsigned();
            $table->foreign('afmeting_id')->references('id')->on('afmetingen');
            $table->string('gewicht', 10);
            $table->integer('spoed')->nullable();
            $table->integer('verzekering')->nullable();
            $table->decimal('prijs');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pakketten');
    }
}
