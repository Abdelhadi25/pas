@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <p>Uw bent verwijderd door meneer Pas neem contact met hem op via email (pas@pas.com)</p>
            </div>
        </div>
    </div>
@endsection
