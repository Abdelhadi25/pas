@extends('layouts.app')
@section('content')

    @if (Session::has('message'))
        <div class="alert alert-success">{{ Session::get('message') }}</div>
    @endif

    <form method="post" action="/status/veranderen">
        @csrf
    <div class="row justify-content-center">
        <table style="width: 95%" class="table">
            <thead>
            <tr>
                <th scope="col">Pakket nummer</th>
                <th scope="col">Naam</th>
                <th scope="col">Status</th>
                <th scope="col">Ophaal straat</th>
                <th scope="col">Ophaal huisnummer</th>
                <th scope="col">Ophaal postcode</th>
                <th scope="col">Ophaal woonplaats</th>
                <th scope="col">Bezorg straat</th>
                <th scope="col">Bezorg huisnummer</th>
                <th scope="col">Bezorg postcode</th>
                <th scope="col">Bezorg woonplaats</th>
                <th scope="col">Afmeting</th>
                <th scope="col">Spoed</th>
                <th scope="col">Verzekering</th>
                <th scope="col">Prijs</th>
                <th scope="col">Opslaan</th>
            </tr>
            </thead>
            <tbody>

            @foreach($data as $item)
                <form method="post" action="/status/veranderen">
                    @csrf
                <tr>
                    <td>{{$item['id']}}</td>
                    <td>{{$item['voornaam']}} {{$item['tussenvoegsel']}} {{$item['achternaam']}}</td>
                    <td><input type="hidden" value="{{$item['id']}}" name="id">
                        <select name="status_id">
                            <option value="{{$item['status_id']}}" selected>{{$item['status']}}</option>
                            <option value="2">Aangenomen</option>
                            <option value="3">Onderweg om op te halen</option>
                            <option value="4">Tijdelijk opgeslagen</option>
                            <option value="5">Ondeerweg om te bezorgen</option>
                            <option value="6">Bezorgd</option>
                            <option value="7">Probleem</option>
                        </select></td>
                    <td>{{$item['ophaal_straat']}}</td>
                    <td>{{$item['ophaal_huisnummer']}}</td>
                    <td>{{$item['ophaal_postcode']}}</td>
                    <td>{{$item['ophaal_woonplaats']}}</td>
                    <td>{{$item['bezorg_straat']}}</td>
                    <td>{{$item['bezorg_huisnummer']}}</td>
                    <td>{{$item['bezorg_postcode']}}</td>
                    <td>{{$item['bezorg_woonplaats']}}</td>
                    <td>{{$item['afmeting']}}</td>
                    <td>{{$item['spoed']}}</td>
                    <td>{{$item['verzekering']}}</td>
                    <td>{{$item['prijs']}}</td>
                    <td><button class="btn btn-danger" type="submit">Opslaan</button></td>
                    <td></td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
    <form>
@endsection
