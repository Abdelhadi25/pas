@extends('layouts.app')

@section('content')
    @if (Session::has('message'))
        <div class="alert alert-success">{{ Session::get('message') }}</div>
    @endif
    <div class="container">
        <div class="row justify-content-center">
            <table class="table">
                <thead>
                <tr>
                    <th scope="col">Voornaam</th>
                    <th scope="col">Tussenvoegsel</th>
                    <th scope="col">Achternaam</th>
                    <th scope="col">Chauffeur stoppen</th>
                </tr>
                </thead>
                <tbody>
                @foreach($data as $item)
                <tr>
{{--                    {{dd($item['id'])}}--}}
                    <td>{{$item['voornaam']}}</td>
                    <td>{{$item['tussenvoegsel']}}</td>
                    <td>{{$item['achternaam']}}</td>
                    <td><a class="btn btn-danger" href="/chauffeur/verwijderen/{{$item['id']}}">Verwijderen</a></td>
                    <td></td>
                </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection
