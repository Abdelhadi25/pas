@extends('layouts.app')

@section('content')
    <div class="row justify-content-center">
        <table style="width: 95%" class="table">
            <thead>
            <tr>
                <th scope="col">Pakket nummer</th>
                <th scope="col">Chauffeur naam</th>
                <th scope="col">Bezorg straat</th>
                <th scope="col">Bezorg huisnummer</th>
                <th scope="col">Bezorg postcode</th>
                <th scope="col">Bezorg woonplaats</th>
                <th scope="col">Afmeting</th>
                <th scope="col">Spoed</th>
                <th scope="col">Verzekering</th>
                <th scope="col">Prijs</th>
            </tr>
            </thead>
            <tbody>
            @foreach($data as $item)
                <tr>
                    <td>{{$item['id']}}</td>
                    <td>{{$item['voornaam']}} {{$item['tussenvoegsel']}} {{$item['achternaam']}}</td>
                    <td>{{$item['bezorg_straat']}}</td>
                    <td>{{$item['bezorg_huisnummer']}}</td>
                    <td>{{$item['bezorg_postcode']}}</td>
                    <td>{{$item['bezorg_woonplaats']}}</td>
                    <td>{{$item['afmeting']}}</td>
                    <td>{{$item['spoed']}}</td>
                    <td>{{$item['verzekering']}}</td>
                    <td>{{$item['prijs']}}</td>
                    <td></td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection
