@extends('layouts.app')

@section('content')
    <div class="row justify-content-center">
        <table style="width: 95%" class="table">
            <thead>
            <tr>
                <th scope="col">Pakket nummer</th>
                <th scope="col">Naam</th>
                <th scope="col">Chauffeur naam</th>
                <th scope="col">Status</th>
                <th scope="col">Ophaal straat</th>
                <th scope="col">Ophaal huisnummer</th>
                <th scope="col">Ophaal postcode</th>
                <th scope="col">Ophaal woonplaats</th>
                <th scope="col">Bezorg straat</th>
                <th scope="col">Bezorg huisnummer</th>
                <th scope="col">Bezorg postcode</th>
                <th scope="col">Bezorg woonplaats</th>
                <th scope="col">Afmeting</th>
                <th scope="col">Spoed</th>
                <th scope="col">Verzekering</th>
                <th scope="col">Prijs</th>
            </tr>
            </thead>
            <tbody>
            @foreach($data as $item)
                <tr>
                    <td>{{$item['id']}}</td>
                    <td>{{$item['klant_voornaam']}} {{$item['klant_tussenvoegsel']}} {{$item['klant_achternaam']}}</td>
                    <td>{{$item['chauffeur_voornaam']}} {{$item['chauffeur_tussenvoegsel']}} {{$item['chauffeur_achternaam']}}</td>
                    <td>{{$item['status']}}</td>
                    <td>{{$item['ophaal_straat']}}</td>
                    <td>{{$item['ophaal_huisnummer']}}</td>
                    <td>{{$item['ophaal_postcode']}}</td>
                    <td>{{$item['ophaal_woonplaats']}}</td>
                    <td>{{$item['bezorg_straat']}}</td>
                    <td>{{$item['bezorg_huisnummer']}}</td>
                    <td>{{$item['bezorg_postcode']}}</td>
                    <td>{{$item['bezorg_woonplaats']}}</td>
                    <td>{{$item['afmeting']}}</td>
                    <td>{{$item['spoed']}}</td>
                    <td>{{$item['verzekering']}}</td>
                    <td>{{$item['prijs']}}</td>
                    <td></td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection
