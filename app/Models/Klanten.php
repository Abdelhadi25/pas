<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Klanten extends Model
{
    use HasFactory;

    protected $table = 'klanten';

    protected $fillable = [
        'id',
        'user_id',
        'straat',
        'huisnummer',
        'postcode',
        'woonplaats',
        'telefoonnummer',
    ];
}
