<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Rollen extends Model
{
    use HasFactory;

    protected $table = 'rollen';

    protected $fillable = [
        'id',
        'rol',
        'created_at',
        'updated_at',
    ];
}
