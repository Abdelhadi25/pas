<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Afmetingen extends Model
{
    use HasFactory;

    protected $table = 'afmetingen';

    protected $fillable = [
        'id',
        'afmeting',
    ];
}
