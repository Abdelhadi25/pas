<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pakketten extends Model
{
    use HasFactory;

    protected $table = 'pakketten';

    protected $fillable = [
        'id',
        'klant_id',
        'status_id',
        'chauffeur_id',
        'ophaal_straat',
        'ophaal_huisnummer',
        'ophaal_postcode',
        'ophaal_woonplaats',
        'bezorg_straat',
        'bezorg_huisnummer',
        'bezorg_postcode',
        'bezorg_woonplaats',
        'afmeting_id    ',
        'gewicht',
        'spoed',
        'verzekering',
        'prijs',
        'created_at',
        'opdated_at',
    ];
}
