<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pakket_status extends Model
{
    use HasFactory;

    protected $table = 'pakket_status';

    protected $fillable = [
        'id',
        'status',
    ];
}
