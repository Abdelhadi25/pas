<?php

namespace App\Http\Controllers;

use App\Models\Pakketten;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Routing\Redirector;


class Chauffeur extends Controller
{
    public function index(){
        return view('chauffeur');
    }

    public function yourDeleted()
    {
        return view('chauffeurDeleted');
    }

    public function aangemeldPakketten()
    {
        $data = Pakketten::select('pakketten.id AS id' ,'voornaam', 'tussenvoegsel', 'achternaam', 'ophaal_straat', 'ophaal_huisnummer', 'ophaal_postcode', 'ophaal_woonplaats', 'bezorg_straat', 'bezorg_huisnummer', 'bezorg_postcode', 'bezorg_woonplaats', 'afmeting','spoed', 'verzekering', 'prijs')
//            ->join('klanten', 'pakketten.klant_id', '=', 'klanten.id')
            ->join('users' , 'users.user_id', 'users.id')
            ->join('afmetingen' , 'pakketten.afmeting_id', 'afmetingen.id')
            ->where('chauffeur_id', '=', NULL)
            ->get();
        return view('chauffeur.aangemeldPakketten', compact('data'));
    }

    public function aangenomenPakketten()
    {
        $data = Pakketten::select('pakketten.id AS id' ,'voornaam', 'tussenvoegsel', 'achternaam', 'status', 'pakketten.status_id AS status_id' ,'ophaal_straat', 'ophaal_huisnummer', 'ophaal_postcode', 'ophaal_woonplaats', 'bezorg_straat', 'bezorg_huisnummer', 'bezorg_postcode', 'bezorg_woonplaats', 'afmeting','spoed', 'verzekering', 'prijs')
            ->join('klanten', 'pakketten.klant_id', '=', 'klanten.id')
            ->join('users' , 'klanten.user_id', 'users.id')
            ->join('afmetingen' , 'pakketten.afmeting_id', 'afmetingen.id')
            ->join('pakket_status' , 'pakketten.status_id', 'pakket_status.id')
            ->where('chauffeur_id', '=', Auth::id())
            ->get();
        return view('chauffeur.aangenomenPakketten', compact('data'));
    }

    public function neemPakket($id)
    {
        Pakketten::where('id', '=', $id)->update(['chauffeur_id' => Auth::id() , 'status_id' => 2]);

        return redirect()->back()->with('message', 'Pakket is aangenomen');
    }

    public function statusVeranderen(Request $request)
    {

        Pakketten::where('id', '=', $request['id'])->update(['status_id' => $request['status_id']]);

        return redirect()->back()->with('message', 'status is verandert');
    }

}
