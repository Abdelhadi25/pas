<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo;
    public function redirectTo()
    {
        if (auth()->user()->actief == 1)
        {
            $this->redirectTo = '/your/deleted';
            return $this->redirectTo;
        }
        if (auth()->user()->rol_id == 1)
        {
            $this->redirectTo = '/klant/dashboard';
            return $this->redirectTo;
        }
        else if (auth()->user()->rol_id == 2)
        {
            $this->redirectTo = '/chauffeur/dashboard';
            return $this->redirectTo;
        }
        else if (auth()->user()->rol_id == 3)
        {
            $this->redirectTo = '/medewerkers/dashboard';
            return $this->redirectTo;
        }
        else
        {
            $this->redirectTo = '/pas/dashboard';
            return $this->redirectTo;
        }
    }

    public function username()
    {
        return 'gebruikersnaam';
    }
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
}
