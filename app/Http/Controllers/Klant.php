<?php

namespace App\Http\Controllers;

use App\Models\Klanten;
use App\Models\Pakket_status;
use App\Models\Pakketten;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class Klant extends Controller
{
    public function index(){
        return view('klant');
    }

    public function getPakket()
    {

      $data = Pakketten::select( 'pakketten.id', 'status' ,'ophaal_straat', 'ophaal_huisnummer', 'ophaal_postcode', 'ophaal_woonplaats', 'bezorg_straat', 'bezorg_huisnummer', 'bezorg_postcode', 'bezorg_woonplaats', 'afmeting','spoed', 'verzekering', 'prijs')
          ->where('klant_id', '=', Auth::id())
          ->join('pakket_status', 'pakket_status.id', '=', 'status_id')
          ->join('afmetingen' , 'pakketten.afmeting_id', 'afmetingen.id')
          ->get();
      return view('klant.mijn-pakket', compact('data'));
    }

    public function addPakket()
    {
        return view('klant.pakket-toevoegen');
    }

    public function store(Request $request)
    {

        $klant_id = Auth::id();
        $pakket = new Pakketten();
        $pakket['klant_id'] = $klant_id;
        $pakket['status_id'] = 1;
        $pakket['ophaal_straat'] = $request->ophaal_straat;
        $pakket['ophaal_huisnummer'] = $request->ophaal_huisnummer;
        $pakket['ophaal_postcode'] = $request->ophaal_postcode;
        $pakket['ophaal_woonplaats'] = $request->ophaal_woonplaats;
        $pakket['bezorg_straat'] = $request->bezorg_straat;
        $pakket['bezorg_huisnummer'] = $request->bezorg_huisnummer;
        $pakket['bezorg_postcode'] = $request->bezorg_postcode;
        $pakket['bezorg_woonplaats'] = $request->bezorg_woonplaats;
        $pakket['afmeting_id'] = $request->afmeting_id;
        $pakket['gewicht'] = $request->gewicht;
        $pakket['spoed'] = $request->spoed;
        $pakket['verzekering'] = $request->verzekering;
        $pakket['prijs'] = 25.20;
        $pakket->save();

        return redirect('mijn-pakketen')->with('message', 'Uw pakket is aangemeld');
    }
}
