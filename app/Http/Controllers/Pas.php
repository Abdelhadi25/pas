<?php

namespace App\Http\Controllers;

use App\Models\Pakketten;
use App\Models\User;
use Illuminate\Http\Request;
use Carbon\Carbon;
use function PHPUnit\Framework\returnSelf;

class Pas extends Controller
{
    public function index()
    {
        $data = User::where('rol_id', '=', 2)->where('actief', '=', 0)->get();
        return view('pas', compact('data'));
    }

    public function overzichtInkomsten()
    {
        $data = Pakketten::select('pakketten.id AS id' ,'voornaam', 'chauffeur_id' ,'tussenvoegsel', 'achternaam', 'pakketten.status_id AS status_id' , 'bezorg_straat', 'bezorg_huisnummer', 'bezorg_postcode', 'bezorg_woonplaats', 'afmeting','spoed', 'verzekering', 'prijs')
            ->whereMonth('pakketten.created_at', '=', Carbon::now()->subMonth()->month)
            ->where('status_id', '=', 6)
            ->join('users' , 'chauffeur_id', 'users.id')
            ->join('pakket_status' , 'pakketten.status_id', 'pakket_status.id')
            ->join('afmetingen' , 'pakketten.afmeting_id', 'afmetingen.id')
//            ->groupby('id', 'chauffeur_id' )
            ->get();
//return $data;
        return view('overzichtInkomsten', compact('data'));
    }

    public function chauffeurDelete($id)
    {
        User::where('id', '=', $id)->update(['actief' => 1]);

        return redirect()->back()->with('message', 'chauffeur is verwijderd');
    }
}
