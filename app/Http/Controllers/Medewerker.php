<?php

namespace App\Http\Controllers;
use App\Models\Pakketten;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Routing\Redirector;

class Medewerker extends Controller
{
    public function index(){
        $data = Pakketten::select('pakketten.id AS id' ,'users.voornaam AS klant_voornaam', 'users.tussenvoegsel AS klant_tussenvoegsel', 'users.achternaam AS klant_achternaam', 'status', 'users2.voornaam AS chauffeur_voornaam', 'users2.tussenvoegsel AS chauffeur_tussenvoegsel', 'users2.achternaam AS chauffeur_achternaam' ,'pakketten.status_id AS status_id' ,'ophaal_straat', 'ophaal_huisnummer', 'ophaal_postcode', 'ophaal_woonplaats', 'bezorg_straat', 'bezorg_huisnummer', 'bezorg_postcode', 'bezorg_woonplaats', 'afmeting','spoed', 'verzekering', 'prijs')
            ->join('users', 'pakketten.klant_id', '=', 'users.id')
            ->join('users AS users2' , 'pakketten.chauffeur_id', 'users2.id')
//            ->join('users' , 'klanten.user_id', 'users.id')
            ->join('afmetingen' , 'pakketten.afmeting_id', 'afmetingen.id')
            ->join('pakket_status' , 'pakketten.status_id', 'pakket_status.id')
            ->get();
        return view('medewerker', compact('data'));
    }

}
